import sbt._

object Dependencies {

  val akkaVersion           = "2.6.16"
  val akkaHttpVersion       = "10.2.4"
  val testcontainersVersion = "0.39.3"

  val compile = Seq(
    "com.typesafe.akka"     %% "akka-actor"           % akkaVersion,
    "com.typesafe.akka"     %% "akka-stream"          % akkaVersion,
    "com.typesafe.akka"     %% "akka-http"            % akkaHttpVersion,
    "com.typesafe.akka"     %% "akka-http-spray-json" % akkaHttpVersion,
    "com.github.pureconfig" %% "pureconfig"           % "0.15.0"
  )

  val runtime = Seq(
    "ch.qos.logback"       % "logback-classic"          % "1.2.3",
    "net.logstash.logback" % "logstash-logback-encoder" % "6.6",
    "org.codehaus.janino"  % "janino"                   % "3.1.3"
  )

  val test = Seq(
    "com.typesafe.akka" %% "akka-testkit"        % akkaVersion,
    "com.typesafe.akka" %% "akka-http-testkit"   % akkaHttpVersion,
    "com.typesafe.akka" %% "akka-stream-testkit" % akkaVersion,
    "org.scalatest"     %% "scalatest"           % "3.2.8" % "it,test",
    "org.scalatestplus" %% "scalacheck-1-14"     % "3.2.2.0",
    "org.scalacheck"    %% "scalacheck"          % "1.15.4",
    "org.scalamock"     %% "scalamock"           % "5.1.0"
  )

  val it = Seq(
    "com.dimafeng" %% "testcontainers-scala-scalatest" % testcontainersVersion
  )
}
