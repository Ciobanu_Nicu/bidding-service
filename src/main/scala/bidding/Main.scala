package bidding

import akka.actor.{ActorRef, ActorSystem}
import akka.http.scaladsl.Http
import akka.http.scaladsl.settings.ServerSettings
import akka.util.Timeout
import bidding.Util.campaigns
import bidding.advertiser.Advertiser

import scala.concurrent.duration._
import bidding.configuration.{Settings, SettingsProvider}
import bidding.publisher.Publisher
import bidding.publisher.Publisher.CreateCampaignRequest
import bidding.route.ApiRoutes
import bidding.service.{BidRepository, BidRepositoryProvider, BidService, BidServiceProvider}

import scala.util.{Failure, Success}
import scala.concurrent.ExecutionContext

object Main extends App with SettingsProvider with BidRepositoryProvider with BidServiceProvider with ApiRoutes {

  implicit override lazy val system: ActorSystem = ActorSystem("main-bidding-service")
  implicit val ec: ExecutionContext              = system.dispatcher
  implicit override val defaultTimeout: Timeout  = Timeout(25 seconds)

  override val advertiser: ActorRef = system.actorOf(Advertiser.props(), "main-advertiser")
  override val publisher: ActorRef  = system.actorOf(Publisher.props(advertiser), "main-publisher")

  implicit override val settings: Settings           = Settings.instance
  implicit override val bidRepository: BidRepository = BidRepository.instance
  implicit override val bidService: BidService       = BidService.instance
  val port                                           = settings.load.map(_.api.port).getOrElse(5000)

  val serverSettings = ServerSettings(system)

  /** Creation campaign request.
    */

  campaigns.foreach(campaign => publisher ! CreateCampaignRequest(campaign))

  val futureBinding = Http()
    .newServerAt(interface = "0.0.0.0", port = port)
    .withSettings(serverSettings)
    .bind(route)

  futureBinding.onComplete {
    case Success(binding) =>
      val address = binding.localAddress
      system.log.info("Server online at http://{}:{}/", address.getHostString, address.getPort)
    case Failure(ex) =>
      system.log.error("Failed to bind HTTP endpoint, terminating system", ex)
      system.terminate()
  }
}
