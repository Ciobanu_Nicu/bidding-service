package bidding.advertiser

import akka.actor.{Actor, ActorLogging, Props}
import bidding.advertiser.Advertiser._
import bidding.model.BidingModel.Campaign
import bidding.model.RequestModel.BidRequests
import bidding.publisher.Publisher._
import bidding.service.{BidRepository, BidRepositoryProvider, BidService, BidServiceProvider}

object Advertiser {
  def props(): Props = Props(new Advertiser())

  case class ReceiveCreationBidRequest(request: BidRequests)
  case class ReceiveCreationCampaignRequest(campaign: Campaign)
}

class Advertiser extends Actor with ActorLogging with BidRepositoryProvider with BidServiceProvider {

  implicit val bidRepository: BidRepository = BidRepository.instance
  implicit val bidService: BidService       = BidService.instance

  override def receive: Receive = {
    /** Adviser actor will receive a creation bid request from Publisher actor. The repository will be called for existing campaigns, after
      * that the response * will be created if bid's match a campaign.
      */

    case ReceiveCreationBidRequest(request) =>
      log.info(s"Processing bidding request $request")
      val campaigns = bidRepository.getAll
      val response  = bidService.createBidResponse(campaigns, request)
      sender() ! CreationBidRequestResponse(response)

    /** Adviser actor will receive a creation campaign request from Publisher actor. The repository will be called for adding campaigns,
      * after that Adviser will respond to Publisher with created campaign.
      */

    case ReceiveCreationCampaignRequest(campaign) =>
      log.info("Processing creation campaign request")
      bidRepository.store(campaign)
      val created = bidRepository.getCampaignById(campaign.id)
      sender() ! CreationCampaignRequestResponse(created)

  }
}
