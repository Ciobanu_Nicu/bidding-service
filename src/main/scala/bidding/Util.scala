package bidding

import bidding.model.BidingModel.{Banner, Campaign, Targeting}
import bidding.model.RequestModel._

object Util {

  val campaign1 = Campaign(
    id = "1001",
    country = "LT",
    targeting = Targeting(
      targetedSiteIds = Seq("0006a522ce0f4bbbbaa6b3c38cafaa0f")
    ),
    banners =
      List(Banner(id = 1001, src = "https://business.eskimi.com/wp-content/uploads/2020/06/openGraph.jpeg", width = 300, height = 250)),
    bid = 5
  )

  val campaign2 = Campaign(
    id = "1002",
    country = "RO",
    targeting = Targeting(
      targetedSiteIds = Seq("0006a522ce0f4bbbbaa6b3c38cafaa1f")
    ),
    banners =
      List(Banner(id = 1002, src = "https://business.eskimi.com/wp-content/uploads/2021/06/openGraph.jpeg", width = 450, height = 400)),
    bid = 6
  )

  val campaigns = List(campaign1, campaign2)

  val bidRequest = BidRequest(
    id = "SGu1Jpq1IO",
    imp = Some(
      List(
        Impression(
          id = "1",
          wmin = Some(50),
          wmax = Some(300),
          hmin = Some(100),
          hmax = Some(300),
          h = Some(250),
          w = Some(300),
          bidFloor = Some(3.12123)
        )
      )
    ),
    site = Site(
      id = "0006a522ce0f4bbbbaa6b3c38cafaa0f",
      domain = "fake.tld"
    ),
    user = Some(
      User(
        id = "USARIO1",
        geo = Some(
          Geo(
            country = Some("LT")
          )
        )
      )
    ),
    device = None
  )

  val bidRequests = BidRequests(bidRequests = List(bidRequest))

  val requestsStr =
    """
      |{
      |  "bidRequests": [{
      |    "id": "SGu1Jpq1IO",
      |    "imp": [{
      |      "bidFloor": 3.12123,
      |      "h": 250,
      |      "hmax": 300,
      |      "hmin": 100,
      |      "id": "1",
      |      "w": 300,
      |      "wmax": 300,
      |      "wmin": 50
      |    }],
      |    "site": {
      |      "domain": "fake.tld",
      |      "id": "0006a522ce0f4bbbbaa6b3c38cafaa0f"
      |    },
      |    "user": {
      |      "geo": {
      |        "country": "LT"
      |      },
      |      "id": "USARIO1"
      |    }
      |  }]
      |}
      |""".stripMargin

}
