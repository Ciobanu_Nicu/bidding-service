package bidding.service

import bidding.model.BidingModel.{Banner, Campaign}
import bidding.model.RequestModel.{BidRequest, BidRequests, Impression}
import bidding.model.ResponseModel.BidResponse

trait BidService {
  def createBidResponse(campaigns: List[Campaign], request: BidRequests): Option[BidResponse]
}

object BidService {

  implicit def instance: BidService = new BidService {

    override def createBidResponse(campaigns: List[Campaign], request: BidRequests): Option[BidResponse] = {
      val collection: List[(Campaign, BidRequest, Option[(Banner, Impression)])] = campaigns.flatMap { campaign =>
        if (request.bidRequests.isEmpty)
          List.empty
        else {
          request.bidRequests.map(request => (campaign, request, getMatchedBanner(campaign, request)))
        }
      }

      if (collection.isEmpty)
        None
      else {
        getWinningCollection(collection) match {
          case Some((campaign, request, (banner, impression))) =>
            Some(
              BidResponse(
                id = request.id,
                bidRequestId = request.id,
                price = impression.bidFloor.getOrElse(campaign.bid),
                adid = Some(campaign.id),
                banner = Some(banner)
              )
            )
          case _ => None
        }
      }
    }
  }

  /** Check if the minimum price is accepted (campaign price >= impression price), and will choose the collections that may have user,
    * device or both(can have none of them). For each collection that has one of the above options(user, device or both), a flag is assigned
    * by their priority. 'device' option has a higher priority than 'user', flag 1 will be assigned for both options, then 2 for the
    * "device" option and 3 for the "user" option
    */

  def getWinningCollection(
      bidCollection: List[(Campaign, BidRequest, Option[(Banner, Impression)])]
  ): Option[(Campaign, BidRequest, (Banner, Impression))] = {

    val colWithBnAndImpOrdByPrice: List[(Campaign, BidRequest, (Banner, Impression))] = bidCollection.collect {
      case (campaign, request, Some((banner, imp @ Impression(_, _, _, _, _, _, _, Some(bidFloor))))) if campaign.bid >= bidFloor =>
        (campaign, request, (banner, imp))
    }

    val colWithFlags: List[(Campaign, BidRequest, (Banner, Impression), Int)] =
      if (colWithBnAndImpOrdByPrice.isEmpty)
        List.empty
      else {
        colWithBnAndImpOrdByPrice.flatMap { value =>
          value._2 match {
            case BidRequest(_, _, _, Some(_), Some(_)) => Some((value._1, value._2, value._3, 1)) // assign flag by priority,
            case BidRequest(_, _, _, None, Some(_))    => Some((value._1, value._2, value._3, 2)) // 1 being the highest priority
            case BidRequest(_, _, _, Some(_), None)    => Some((value._1, value._2, value._3, 3))
            case _                                     => None
          }
        }
      }

    val orderedColByPriority: List[(Campaign, BidRequest, (Banner, Impression))] =
      if (colWithFlags.isEmpty)
        List.empty
      else {
        colWithFlags.sortBy(_._4).map(winCol => (winCol._1, winCol._2, winCol._3))
      }

    orderedColByPriority.headOption
  }

  /** Bid request site id must be in the campaign targeting id's
    */

  def getMatchedBanner(campaign: Campaign, request: BidRequest): Option[(Banner, Impression)] = {
    val idsMatch = campaign.targeting.targetedSiteIds.contains(request.site.id)

    val bnsWithImps: List[(Banner, Option[Impression])] =
      if (!idsMatch)
        List.empty
      else {
        campaign.banners.flatMap { banner =>
          val matchedImpressions = request.imp match {
            case Some(impressions) =>
              getMatchedImpressions(banner, impressions)
            case None => List.empty
          }

          matchedImpressions match {
            case Nil         => None
            case impressions => Some((banner, ltImpByPrice(impressions)))
          }
        }
      }

    if (bnsWithImps.isEmpty)
      None
    else
      ltBnByImpPrice(bnsWithImps)
  }

  /** A banner can have one, more or no impression. For banners that have at least one impression, is chosen the impressions whose sizes
    * match the banner sizes. Always prefer validating width and height if they exist, otherwise fallback to width min, width max, height
    * min and height max.
    *
    * case 1: When impression have width and height fields impression width <= banner width and impression height <= banner height;
    *
    * case 2: When impression have width min, width max, height min and height max fields(no width and height fields) impression width min
    * >= banner width or impression width max >= banner width and impression height min <= banner height or impression height max >= banner
    * height;
    *
    * case 3: When impression have width min, width max and height filed (no height min and height max fields) impression width min >=
    * banner width or impression width max >= banner width and impression height <= banner height;
    *
    * case 4: When impression have width, height min and height max fields(no width min and width max fields) impression width <= banner
    * width and impression height min <= banner height or impression height max >= banner height;
    *
    * case 5: When impression have all size fields addresses the above cases.
    */

  private def getMatchedImpressions(bn: Banner, impressions: List[Impression]): List[Impression] = {
    impressions.collect { case imp @ Impression(_, widthMin, widthMax, width, heightMin, heightMax, height, _) =>
      (widthMin, widthMax, width, heightMin, heightMax, height) match {
        case (_, _, Some(w), _, _, Some(h)) if w <= bn.width && h <= bn.height => Some(imp)
        case (Some(wMin), Some(wMax), None, Some(hMin), Some(hMax), None)
            if (wMin <= bn.width || wMax >= bn.width) && (hMin <= bn.height || hMax >= bn.height) =>
          Some(imp)
        case (Some(wMin), Some(wMax), None, _, _, Some(h)) if (wMin <= bn.width || wMax >= bn.width) && (h <= bn.height) => Some(imp)
        case (_, _, Some(w), Some(hMin), Some(hMax), None) if w <= bn.width && (hMin <= bn.height || hMax >= bn.height)  => Some(imp)
        case (Some(wMin), Some(wMax), Some(w), Some(hMin), Some(hMax), Some(h))
            if (w <= bn.width && h <= bn.height) || ((wMin <= bn.width || wMax >= bn.width) && (hMin <= bn.height || hMax >= bn.height)) ||
              ((wMin <= bn.width || wMax >= bn.width) && (h <= bn.height)) || (w <= bn.width && (hMin <= bn.height || hMax >= bn.height)) =>
          Some(imp)
        case (_, _, _, _, _, _) => None
      }
    }.flatten
  }

  /** Every impression has a price, from an impression list is choose the impression with the lowest price.
    */

  private def ltImpByPrice(impressions: List[Impression]): Option[Impression] = {
    impressions match {
      case Nil => None
      case list =>
        val ordered = list.sortWith((current, next) => Ordering[Option[Double]].lt(current.bidFloor, next.bidFloor))

        ordered.headOption
    }
  }

  /** A banner can have one, more or no impression. For banners that have at least one impression, is chosen the banner that has the
    * impression with the lowest price.
    */

  private def ltBnByImpPrice(bnsWithImps: List[(Banner, Option[Impression])]): Option[(Banner, Impression)] = {
    val pairs = bnsWithImps.collect { case (banner, Some(impression)) => (banner, impression) }

    if (pairs.isEmpty)
      None
    else {
      val ordered = pairs.sortWith((current, next) => Ordering[Option[Double]].lt(current._2.bidFloor, next._2.bidFloor))
      ordered.headOption
    }
  }
}

trait BidServiceProvider {
  val bidService: BidService
}
