package bidding.service

import bidding.model.BidingModel.Campaign

import scala.collection.mutable

trait BidRepository {
  def store(campaign: Campaign): Option[Campaign]
  def getCampaignById(campaignId: String): Option[Campaign]
  def getAll: List[Campaign]
}

object BidRepository {
  implicit def instance: BidRepository = new BidRepository {

    val storage: mutable.Map[String, Campaign] = mutable.Map.empty

    override def store(campaign: Campaign): Option[Campaign] = {
      if (!storage.contains(campaign.id))
        storage.put(campaign.id, campaign)
      else None
    }

    override def getCampaignById(campaignId: String): Option[Campaign] =
      storage.values.find(_.id == campaignId)

    override def getAll: List[Campaign] = storage.values.toList
  }
}

trait BidRepositoryProvider {
  val bidRepository: BidRepository
}
