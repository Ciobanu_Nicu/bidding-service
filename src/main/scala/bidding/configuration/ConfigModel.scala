package bidding.configuration

import pureconfig.error._

object ConfigModel {

  case class AppConfig(api: Api)

  case class Api(port: Int)

  case class ConfigError(failure: ConfigReaderFailures) extends Exception {

    private def failureToString(failure: ConfigReaderFailure) = failure match {
      case _: CannotParse | _: CannotReadFile => failure.description
      case f: ConvertFailure                  => s"${f.description} in ${f.path}"
      case f: ThrowableFailure                => s"${f.description} \n ${f.throwable.getMessage}"
    }

    override def getMessage: String = failure.toList.map(failureToString).mkString("\n")
  }
}
