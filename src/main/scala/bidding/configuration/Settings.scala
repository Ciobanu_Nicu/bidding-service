package bidding.configuration

import com.typesafe.config.ConfigFactory
import pureconfig.ConfigSource
import pureconfig.error.ConfigReaderFailures
import pureconfig.generic.auto._
import bidding.configuration.ConfigModel._

trait Settings {
  def load: Option[AppConfig]
}

object Settings {
  implicit def instance: Settings = new Settings {
    override def load: Option[AppConfig] =
      conf match {
        case Left(e) =>
          new Exception(e.toString())
          None
        case Right(config) => Some(config)
      }

    private def conf: Either[ConfigReaderFailures, AppConfig] =
      ConfigSource
        .fromConfig(
          ConfigFactory
            .load("application.conf")
            .getConfig("bidding")
        )
        .load[AppConfig]
  }
}

trait SettingsProvider {
  val settings: Settings
}
