package bidding.route

import akka.actor.{ActorRef, ActorSystem}
import akka.http.scaladsl.model.StatusCodes.{BadRequest, NoContent, OK}
import akka.http.scaladsl.server.{Directives, Route}
import akka.pattern.ask
import akka.util.Timeout
import bidding.advertiser.Advertiser
import bidding.model.BidJsonProtocol

import scala.concurrent.duration._
import bidding.model.RequestModel.BidRequests
import bidding.publisher.Publisher
import bidding.publisher.Publisher.{CreateBidRequest, CreationBidRequestResponse}
import spray.json._

import scala.concurrent.ExecutionContext

trait ApiRoutes extends Directives with BidJsonProtocol {

  implicit lazy val system: ActorSystem = ActorSystem("route-bidding-service")
  implicit val ex: ExecutionContext     = system.dispatcher

  implicit def defaultTimeout: Timeout = Timeout(25 seconds)

  val advertiser: ActorRef = system.actorOf(Advertiser.props(), "route-advertiser")
  val publisher: ActorRef  = system.actorOf(Publisher.props(advertiser), "route-publisher")

  val route: Route =
    pathPrefix("api" / "bid") {
      getWinnerBid
    }

  // POST api/bid/winner

  def getWinnerBid: Route =
    (path("winner") & post) {
      entity(as[BidRequests]) { request =>
        /** Creation bid request.
          */
        onSuccess(publisher ? CreateBidRequest(request)) {
          case CreationBidRequestResponse(Some(response)) =>
            response match {
              case bidResponse if bidResponse.adid.isDefined && bidResponse.banner.isDefined =>
                complete(OK, bidResponse.toJson.prettyPrint)
              case _ => complete(NoContent)
            }
          case CreationBidRequestResponse(None) => complete(NoContent)
          case _                                => complete(BadRequest)
        }
      }
    }
}
