package bidding.publisher

import akka.actor._
import akka.pattern._
import akka.util.Timeout
import bidding.model.BidingModel.Campaign
import bidding.model.RequestModel.BidRequests
import bidding.model.ResponseModel.BidResponse

import scala.concurrent.ExecutionContext

object Publisher {
  def props(advertiser: ActorRef)(implicit timeout: Timeout, ec: ExecutionContext): Props = Props(new Publisher(advertiser))

  case class CreateCampaignRequest(campaign: Campaign)
  case class CreationCampaignRequestResponse(campaign: Option[Campaign])

  case class CreateBidRequest(bidRequest: BidRequests)
  case class CreationBidRequestResponse(bidResponse: Option[BidResponse])
}

class Publisher(advertiser: ActorRef)(implicit timeout: Timeout, ec: ExecutionContext) extends Actor with ActorLogging {

  import Publisher._
  import bidding.advertiser.Advertiser._

  override def receive: Receive = {

    /** Publisher actor will receive a creation bid request from ApiRoutes. After that the bid request will be sent to Adviser actor and
      * will wait for response, which is created from matched campaigns if they exist(it may be an answer with no match - None). The
      * Publisher's response will get back to the ApiRoutes.
      */

    case CreateBidRequest(request) =>
      log.info(s"Sending creation bid request $request")
      val response = (advertiser ? ReceiveCreationBidRequest(request)).mapTo[CreationBidRequestResponse]
      response.pipeTo(sender())

    /** Publisher actor will receive a creation campaign request from Main. After that the creation campaign request will be sent to
      * Adviser, it will add the campaign to the repository and and return the added campaign. The Publisher's response will get back to the
      * Main.
      */

    case CreateCampaignRequest(campaign) =>
      log.info(s"Sending creation campaign request $campaign")
      val response = (advertiser ? ReceiveCreationCampaignRequest(campaign)).mapTo[CreationCampaignRequestResponse]
      response.pipeTo(sender())

  }
}
