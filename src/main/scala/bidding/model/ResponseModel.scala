package bidding.model

import bidding.model.BidingModel.Banner

object ResponseModel {

  case class BidResponse(id: String, bidRequestId: String, price: Double, adid: Option[String], banner: Option[Banner])

}
