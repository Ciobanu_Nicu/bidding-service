package bidding.model

object BidingModel {

  case class Campaign(id: String, country: String, targeting: Targeting, banners: List[Banner], bid: Double)

  case class Banner(id: Int, src: String, width: Int, height: Int)

  sealed trait Target
  case class Targeting(targetedSiteIds: Seq[String]) extends Target

}
