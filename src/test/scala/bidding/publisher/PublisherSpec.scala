package bidding.publisher

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.pattern.ask
import akka.testkit.{DefaultTimeout, TestActorRef, TestKit}
import bidding.StopSystem
import bidding.TestUtil._
import bidding.advertiser.Advertiser
import bidding.advertiser.Advertiser.{ReceiveCreationBidRequest, ReceiveCreationCampaignRequest}
import bidding.model.RequestModel.BidRequests
import bidding.publisher.Publisher.{CreateBidRequest, CreateCampaignRequest, CreationBidRequestResponse, CreationCampaignRequestResponse}
import org.scalatest.funspec.AnyFunSpecLike
import org.scalatest.matchers.should.Matchers

import scala.concurrent.{Await, ExecutionContext}
import scala.concurrent.duration.Duration

class PublisherSpec extends TestKit(ActorSystem("PublisherTest")) with Matchers with AnyFunSpecLike with DefaultTimeout with StopSystem {

  implicit val executionContext: ExecutionContext = system.dispatcher

  val advertiser: ActorRef = system.actorOf(
    Props(
      new Advertiser {
        override def receive: Receive = {
          case ReceiveCreationBidRequest(r) if r.bidRequests.head.id == "none_id" => sender() ! CreationBidRequestResponse(None)
          case ReceiveCreationBidRequest(_)      => sender() ! CreationBidRequestResponse(Some(bidResponse))
          case ReceiveCreationCampaignRequest(_) => sender() ! CreationCampaignRequestResponse(Some(campaign))
        }
      }
    )
  )

  val publisher: ActorRef = TestActorRef(new Publisher(advertiser))

  describe("Publisher") {
    it("should respond to creation campaign request") {
      val future   = publisher ? CreateCampaignRequest(campaign)
      val response = Await.result(future.mapTo[CreationCampaignRequestResponse], Duration.Inf)
      response shouldBe CreationCampaignRequestResponse(Some(campaign))
    }

    it("should respond to creation bid request") {
      val future                               = publisher ? CreateBidRequest(bidRequests)
      val response: CreationBidRequestResponse = Await.result(future.mapTo[CreationBidRequestResponse], Duration.Inf)
      response shouldBe CreationBidRequestResponse(Some(bidResponse))
    }

    it("should respond to creation bid request with None when no match") {
      val request = BidRequests(
        bidRequests = List(bidRequest.copy(id = "none_id"))
      )
      val future                               = publisher ? CreateBidRequest(request)
      val response: CreationBidRequestResponse = Await.result(future.mapTo[CreationBidRequestResponse], Duration.Inf)
      response shouldBe CreationBidRequestResponse(None)
    }
  }
}
