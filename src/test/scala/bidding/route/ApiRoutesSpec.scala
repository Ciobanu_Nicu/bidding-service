package bidding.route

import akka.actor.{ActorRef, Props}
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.util.Timeout
import bidding.TestUtil
import bidding.TestUtil.{bidRequest, bidRequests, bidResponse}
import bidding.advertiser.Advertiser
import bidding.model.BidingModel.Campaign
import bidding.model.RequestModel.BidRequests
import bidding.model.{BidJsonProtocol, BidingModel, RequestModel, ResponseModel}
import bidding.publisher.Publisher
import bidding.publisher.Publisher.{CreateBidRequest, CreationBidRequestResponse, CreationCampaignRequestResponse}
import bidding.service.{BidRepository, BidService}
import org.scalatest.flatspec.AnyFlatSpec

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.DurationInt
import spray.json._

class ApiRoutesSpec extends AnyFlatSpec with ScalatestRouteTest with BidJsonProtocol {

  "ApiRoutes" should
    "return OK on POST /api/bid/winner request" in new TestSubject {
      val request = Post("/api/bid/winner", bidRequests)
      request ~> Route.seal(routes) ~> check {
        assert(status == StatusCodes.OK)
        assert(entityAs[String] == bidResponse.toJson.prettyPrint)
      }
    }

  it should "return NoContent on POST /api/bid/winner request" in new TestSubject {
    val bid = BidRequests(
      bidRequests = List(bidRequest.copy(id = "none_id"))
    )
    val request = Post("/api/bid/winner", bid)
    request ~> Route.seal(routes) ~> check {
      assert(status == StatusCodes.NoContent)
    }
  }

  it should "return BadRequest on POST /api/bid/winner request" in new TestSubject {
    val bid = BidRequests(
      bidRequests = List(bidRequest.copy(id = "bad_id"))
    )
    val request = Post("/api/bid/winner", bid)
    request ~> Route.seal(routes) ~> check {
      assert(status == StatusCodes.BadRequest)
    }
  }

  trait TestSubject {
    val routes: Route = new ApiRoutes {

      implicit override val ex: ExecutionContext = system.dispatcher

      implicit override def defaultTimeout: Timeout = Timeout(25 seconds)

      override val advertiser: ActorRef = system.actorOf(Props(new Advertiser {

        implicit override val bidRepository: BidRepository = new BidRepository {
          override def store(campaign: BidingModel.Campaign): Option[BidingModel.Campaign] = None

          override def getCampaignById(campaignId: String): Option[BidingModel.Campaign] = None

          override def getAll: List[Campaign] = List.empty
        }

        implicit override val bidService: BidService = new BidService {
          override def createBidResponse(campaigns: List[Campaign], request: RequestModel.BidRequests): Option[ResponseModel.BidResponse] =
            None
        }
      }))

      override val publisher: ActorRef = system.actorOf(Props(new Publisher(advertiser) {
        override def receive: Receive = {
          case CreateBidRequest(request) if request.bidRequests.exists(req => req.id == "none_id") =>
            sender() ! CreationBidRequestResponse(None)
          case CreateBidRequest(request) if request.bidRequests.exists(req => req.id == "bad_id") =>
            sender() ! CreationCampaignRequestResponse(None)
          case CreateBidRequest(_) => sender() ! CreationBidRequestResponse(Some(TestUtil.bidResponse))
        }
      }))
    }.route
  }
}
