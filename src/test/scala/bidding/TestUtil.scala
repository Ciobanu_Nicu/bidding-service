package bidding

import bidding.model.BidingModel.{Banner, Campaign, Targeting}
import bidding.model.RequestModel._
import bidding.model.ResponseModel.BidResponse

object TestUtil {

  val campaign = Campaign(
    id = "1001",
    country = "LT",
    targeting = Targeting(
      targetedSiteIds = Seq("0006a522ce0f4bbbbaa6b3c38cafaa0f")
    ),
    banners =
      List(Banner(id = 1001, src = "https://business.eskimi.com/wp-content/uploads/2020/06/openGraph.jpeg", width = 300, height = 250)),
    bid = 5
  )

  val bidRequest = BidRequest(
    id = "SGu1Jpq1IO",
    imp = Some(
      List(
        Impression(
          id = "1",
          wmin = Some(50),
          wmax = Some(300),
          hmin = Some(100),
          hmax = Some(300),
          h = Some(250),
          w = Some(300),
          bidFloor = Some(3.12123)
        )
      )
    ),
    site = Site(
      id = "0006a522ce0f4bbbbaa6b3c38cafaa0f",
      domain = "fake.tld"
    ),
    user = Some(
      User(
        id = "USARIO1",
        geo = Some(
          Geo(
            country = Some("LT")
          )
        )
      )
    ),
    device = None
  )

  val bidRequests = BidRequests(bidRequests = List(bidRequest))

  val bidResponse = BidResponse(
    id = "SGu1Jpq1IO",
    price = 5,
    bidRequestId = "SGu1Jpq1IO",
    adid = Some("1001"),
    banner = Some(
      Banner(
        height = 250,
        id = 1001,
        src = "https://business.eskimi.com/wp-content/uploads/2020/06/openGraph.jpeg",
        width = 300
      )
    )
  )
}
