package bidding.advertiser

import akka.actor.{ActorRef, ActorSystem}
import akka.testkit.{TestActorRef, TestKit, TestProbe}
import bidding.StopSystem
import bidding.TestUtil._
import bidding.advertiser.Advertiser.{ReceiveCreationBidRequest, ReceiveCreationCampaignRequest}
import bidding.model.{BidingModel, RequestModel, ResponseModel}
import bidding.model.BidingModel.Campaign
import bidding.model.RequestModel.BidRequests
import bidding.model.ResponseModel.BidResponse
import bidding.publisher.Publisher.{CreationBidRequestResponse, CreationCampaignRequestResponse}
import bidding.service.{BidRepository, BidService}
import org.scalatest.matchers.should.Matchers
import org.scalatest.funspec.AnyFunSpecLike

class AdvertiserSpec extends TestKit(ActorSystem("AdvertiserTest")) with Matchers with AnyFunSpecLike with StopSystem {

  val advertiser: ActorRef = TestActorRef(new Advertiser {

    implicit override val bidRepository: BidRepository = new BidRepository {
      override def store(campaign: BidingModel.Campaign): Option[BidingModel.Campaign] = Some(campaign)

      override def getCampaignById(campaignId: String): Option[BidingModel.Campaign] = Some(campaign)

      override def getAll: List[Campaign] = List(campaign)
    }

    implicit override val bidService: BidService = new BidService {
      override def createBidResponse(campaigns: List[Campaign], request: RequestModel.BidRequests): Option[ResponseModel.BidResponse] = {
        val condition = request.bidRequests.exists(req => req.id != "none_id")
        if (condition)
          campaigns.headOption.map { campaign =>
            request.bidRequests.map { bidRequest =>
              BidResponse(
                id = bidRequest.id,
                bidRequestId = bidRequest.id,
                price = campaign.bid,
                adid = Some(campaign.id),
                banner = campaign.banners.headOption
              )
            }.head
          }
        else None
      }
    }
  })

  describe("Advertiser") {

    it("should respond to receiving creation campaign request") {
      val probe = TestProbe()
      probe.send(advertiser, ReceiveCreationCampaignRequest(campaign))
      probe.expectMsgType[CreationCampaignRequestResponse] shouldBe CreationCampaignRequestResponse(Some(campaign))
    }

    it("should respond to receiving creation bid request") {
      val probe = TestProbe()
      probe.send(advertiser, ReceiveCreationBidRequest(bidRequests))
      probe.expectMsgType[CreationBidRequestResponse] shouldBe CreationBidRequestResponse(Some(bidResponse))
    }

    it("should respond to receiving creation bid request with None when no match") {
      val request = BidRequests(
        bidRequests = List(bidRequest.copy(id = "none_id"))
      )
      val probe = TestProbe()
      probe.send(advertiser, ReceiveCreationBidRequest(request))
      probe.expectMsgType[CreationBidRequestResponse] shouldBe CreationBidRequestResponse(None)
    }
  }
}
